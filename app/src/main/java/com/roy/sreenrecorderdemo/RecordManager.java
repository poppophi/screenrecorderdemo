package com.roy.sreenrecorderdemo;

import android.app.Activity;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.os.Environment;
import android.os.FileObserver;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;

import java.io.File;
import java.io.IOException;

import androidx.annotation.Nullable;

/**
 * control record function and parameters
 */
class RecordManager {
    private static final String TAG = RecordManager.class.getSimpleName();
    private static final String SAVE_PATH = Environment.getExternalStoragePublicDirectory(
            android.os.Environment.DIRECTORY_DOWNLOADS) + "/My_Record.mp4";
    private MediaRecorder mMediaRecorder;
    private VirtualDisplay mVirtualDisplay;
    private DisplayMetrics mDisplayMetrics;
    private OnRecordFileWriteFinish mOnRecordFileWriteFinish;
    private RecordFileObserver recordFileObserver;

    interface OnRecordFileWriteFinish {
        void onFinish();
    }

    /**
     * 初始化錄影物件
     */
    public void prepareToRecord(MediaProjection mediaProjection, DisplayMetrics displayMetrics) {
        mDisplayMetrics = displayMetrics;
        Log.v(TAG, "video save path " + SAVE_PATH);

        mMediaRecorder = new MediaRecorder();
        try {
            //特別注MediaRecorder的設定順序，請依照以下順序進行設定。
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mMediaRecorder.setVideoSize(mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
            mMediaRecorder.setVideoFrameRate(30);
            mMediaRecorder.setOutputFile(SAVE_PATH);
            mMediaRecorder.setVideoEncodingBitRate(1024 * 1024);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mVirtualDisplay = createVirtualDisplay(mediaProjection);
    }

    /**
     * 開始錄製
     */
    public void startRecord(OnRecordFileWriteFinish onRecordFileWriteFinish) {
        mOnRecordFileWriteFinish = onRecordFileWriteFinish;
        recordFileObserver = new RecordFileObserver(SAVE_PATH, FileObserver.CLOSE_WRITE);
        recordFileObserver.startWatching();
        if (mMediaRecorder != null) {
            mMediaRecorder.start();
        }
    }

    /**
     * 停止錄製
     */
    public void stopRecord() {
        if (mVirtualDisplay != null) {
            mVirtualDisplay.release();
        }
        if (mMediaRecorder != null) {
            mMediaRecorder.stop();
            mMediaRecorder.reset();
        }
    }

    public void destroyRecord() {
        if (mMediaRecorder != null) {
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }

    /**
     * 播放錄製影片
     */
    public void playVideo(Activity activity) {
        File file = new File(SAVE_PATH);
        if (file.exists()) {
            //有檔案才播放
            /*
            Uri photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(photoURI,"video/*");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (mWeakReference.get() != null){
                mWeakReference.get().startActivity(intent);
            }
             */

            //透過VideoView播放
            Intent intent = new Intent(activity, PlayVideoActivity.class);
            intent.putExtra(PlayVideoActivity.VIDEO_PATH, SAVE_PATH);
            activity.startActivity(intent);

        }
    }

    private VirtualDisplay createVirtualDisplay(MediaProjection projection) {
        Surface surface = mMediaRecorder.getSurface();
        return projection.createVirtualDisplay(
                "record screen",
                mDisplayMetrics.widthPixels,
                mDisplayMetrics.heightPixels,
                mDisplayMetrics.densityDpi,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC,
                surface, null, null);
    }

    /**
     * 清除檔案．
     */
    public void clear() {
        File file = new File(SAVE_PATH);
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class RecordFileObserver extends FileObserver {

        public RecordFileObserver(String path, int mask) {
            super(path, mask);
        }

        @Override
        public void onEvent(int event, @Nullable String path) {
            if (mOnRecordFileWriteFinish != null) {
                mOnRecordFileWriteFinish.onFinish();
            }
            recordFileObserver.stopWatching();
        }
    }

}
