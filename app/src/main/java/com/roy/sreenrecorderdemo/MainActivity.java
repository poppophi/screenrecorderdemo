package com.roy.sreenrecorderdemo;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;

/**
 * Android Mobile Screen Record Demo Project.
 * 此專案使用CameraX套件實作前相機畫面的預覽，並使用MediaRecorder進行手機的畫面錄製．
 */
public class MainActivity extends BaseActivity{
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_MEDIA_PROJECTION = 123;

    private Button btnStartRecord, btnStopRecord, btnPlay;
    //CameraX preview物件
    private PreviewView mPreviewView;
    private MediaProjectionManager mMediaProjectionManager;
    private MediaProjection mMediaProjection;
    private RecordManager mRecordManager;
    private MyMediaProjectionCallBack mMyMediaProjectionCallBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecordManager = new RecordManager();
        mRecordManager.clear();
        findView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
    }

    private void findView(){
        btnStartRecord = findViewById(R.id.btn_start_record);
        btnStopRecord = findViewById(R.id.btn_stop_record);
        btnPlay = findViewById(R.id.btn_play);
        mPreviewView = findViewById(R.id.camerax_previewView);

        //Start Record Button
        btnStartRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readyToRecord();
            }
        });

        //Stop Record Button
        btnStopRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mRecordManager != null){
                    mRecordManager.stopRecord();
                }
            }
        });

        //Play Video Button
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mRecordManager != null){
                    mRecordManager.playVideo(MainActivity.this);
                }
            }
        });
        btnPlay.setClickable(false);
    }

    private void checkPermission(){
        boolean isAllGranted = true;
        for(String permission : PERMISSIONS)
        if(checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED){
            isAllGranted = false;
            break;
        }
        if(!isAllGranted){
            requestPermission(PERMISSIONS, new PermissionCallBack() {
                @Override
                public void onSuccess() {
                    startCamera();
                }

                @Override
                public void onFail() {
                    showAppSettingPage();
                }
            });
        }else{
            startCamera();
        }
    }

    /**
     * 啟動相機預覽
     */
    private void startCamera(){
        //異步執行ProcessCameraProvider.getInstance
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        //新增監聽，待ProcessCameraProvider.getInstance執行完成後run以下步驟。
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                //Preview
                Preview preview = new Preview.Builder().build();
                //Sets a Preview.SurfaceProvider to provide a Surface for Preview.
                preview.setSurfaceProvider(mPreviewView.createSurfaceProvider());
                // Used to bind the lifecycle of cameras to the lifecycle owner
                try {
                    //取得異步執行ProcessCameraProvider.getInstance的執行結果
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    cameraProvider.unbindAll();
                    //綁定生命週期並且使用前相機,綁定的生命週期也可以自訂
                    cameraProvider.bindToLifecycle(MainActivity.this, CameraSelector.DEFAULT_FRONT_CAMERA,preview);

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    private void readyToRecord(){
        mMyMediaProjectionCallBack = new MyMediaProjectionCallBack();
        mMediaProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        startActivityForResult(mMediaProjectionManager.createScreenCaptureIntent(),REQUEST_MEDIA_PROJECTION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_MEDIA_PROJECTION && resultCode == RESULT_OK){
            mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
            mMediaProjection.registerCallback(mMyMediaProjectionCallBack,null);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            mRecordManager.prepareToRecord(mMediaProjection, displayMetrics);
            mRecordManager.startRecord(() -> runOnUiThread(() -> {
                Toast.makeText(MainActivity.this,"File Write Finish",Toast.LENGTH_SHORT).show();
                btnPlay.setClickable(true);
            }));
        }
    }

    private void unregisterMediaProjectionCallback(){
        if (mMediaProjection != null){
            mMediaProjection.unregisterCallback(mMyMediaProjectionCallBack);
            mMediaProjection = null;
        }
        mRecordManager.destroyRecord();
    }

    class MyMediaProjectionCallBack extends MediaProjection.Callback{

        @Override
        public void onStop(){
            //MediaProjection.stop() 呼叫後觸發callback
            unregisterMediaProjectionCallback();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mMediaProjection != null){
            mMediaProjection.stop();
        }
        finishAndRemoveTask();
    }
}