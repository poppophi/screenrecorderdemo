package com.roy.sreenrecorderdemo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    public static final int PERMISSION_REQUEST_CODE = 111;

    public static final String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO};

    public interface PermissionCallBack{
        void onSuccess();
        void onFail();
    }

    private PermissionCallBack mPermissionCallBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 檢查使用者權限
     */
    public void requestPermission(String[] permissions, PermissionCallBack permissionCallBack){
        mPermissionCallBack = permissionCallBack;
        requestPermissions(permissions, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_REQUEST_CODE){
            boolean isAllGranted = true;
            for(int result : grantResults){
                if(result != PackageManager.PERMISSION_GRANTED){
                    isAllGranted = false;
                    break;
                }
            }
            if(isAllGranted){
                if (mPermissionCallBack != null)
                    mPermissionCallBack.onSuccess();
            }else{
                if (mPermissionCallBack != null)
                    mPermissionCallBack.onFail();
            }
            mPermissionCallBack = null;
        }
    }

    public void showAppSettingPage(){
        //導到設定頁面
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Need Permission");
        builder.setPositiveButton("Grant now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Fuck yourself", null);
        builder.show();
    }
}
