package com.roy.sreenrecorderdemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;


public class PlayVideoActivity extends AppCompatActivity {
    private static final String TAG = PlayVideoActivity.class.getSimpleName();

    public static final String VIDEO_PATH = "VIDEO_PATH";

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        mVideoView = findViewById(R.id.videoView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if(intent.getExtras().containsKey(VIDEO_PATH) && !TextUtils.isEmpty(intent.getStringExtra(VIDEO_PATH))){
            String path = intent.getStringExtra(VIDEO_PATH);
            // create an object of media controller
            MediaController mediaController = new MediaController(this);
            mVideoView.setVideoURI(Uri.parse(path));
            mVideoView.setMediaController(mediaController);
            mVideoView.start();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}